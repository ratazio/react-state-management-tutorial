import React from "react";
import shallow from 'zustand/shallow';
import "./nicknameHistory.scss";
import UserStore, { withUserStore } from "../../store/userStore.js";
import RenderCounter from "../renderCounter/renderCounter.js";

class NicknameHistory extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const { purchaseHistory, removeProduct } = this.props.store;

    console.log("render to purchaseHistory: ", purchaseHistory);
    return (
      <div className="nickname-history">
        <RenderCounter bottomLeft={true} />
        <h1>History</h1>
        {purchaseHistory.map(function (item, i) {
          return (
            <div className="line">
              <p key={i}>
                ID: {item.id} Name: {item.name} Price: {item.price}
              </p>
              <button
                onClick={() => {
                  removeProduct(item.id);
                }}
              >
                Delete
              </button>
            </div>
          );
        })}
      </div>
    );
  }
}



const mapStateToProps = (BaseComponent) => (props) => {  
  const [purchaseHistory, removeProduct] = UserStore(state => [state.purchaseHistory, state.removeProduct], shallow);  
  return <BaseComponent {...props} store={{purchaseHistory, removeProduct}} />;
};


export const mapAllStateToProps = (BaseComponent) => (props) => {
  const store = UserStore();
  return <BaseComponent {...props} store={store} />;
};


const NicknameHistoryContainer = mapAllStateToProps(NicknameHistory);
export default NicknameHistoryContainer;
