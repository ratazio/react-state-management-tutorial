import React from "react";
import shallow from 'zustand/shallow';
import "./editUser.scss";
import UserStore, {withUserStore} from "../../store/userStore.js";
import RenderCounter from "../renderCounter/renderCounter.js";

class EditUser extends React.Component {
  constructor() {
    super();
    this.state = {
      username: "Username 123",
      userage: "23",
    };
  }


  render() {
    const {setName, setAge} = this.props.store;

    return (
      <div className="edit-user">
        {<RenderCounter />}
        <h1>User</h1>
        <br></br>
        <div className="item">
          <label>Name</label>
          <input
            type="text"
            placeholder="Username 123"
            ref={(inputName) => {
              this.inputNameRef = inputName;
            }}
          ></input>
          <button
            onClick={() => {
              setName(this.inputNameRef.value);
            }}
          >
            Save
          </button>
        </div>
        <br />
        <div className="item">
          <label>Age</label>
          <input
            type="text"
            placeholder="23"
            ref={(inputAge) => {
              this.inputAgeRef = inputAge;
            }}
          ></input>
          <button
            onClick={() => {
              setAge(this.inputAgeRef.value);
            }}
          >
            Save
          </button>
        </div>
      </div>
    );
  }
}




const mapStateToProps = (BaseComponent) => (props) => {
  const [setName, setAge] = UserStore(state => [state.setName, state.setAge], shallow);  
  return <BaseComponent {...props} store={{setName, setAge}} />;
};


const EditUserContainer = mapStateToProps(EditUser);
export default EditUserContainer;