import React from "react";

import "./editPurchases.scss";
import shallow from 'zustand/shallow'
import UserStore, { withUserStore } from "../../store/userStore.js";
import RenderCounter from "../renderCounter/renderCounter.js";

class EditPurchases extends React.Component {
  constructor() {
    super();
    this.state = {
      username: "Username 123",
      userage: "23",
    };
  }

  onNicknameClick() {
    console.log("onNicknameClick ", this.inputNicknameRef.value);
  }

  render() {
    const {addProduct} = this.props.store;
    return (
      <div className="edit-purchases">
        {<RenderCounter />}

        <h1>Product</h1>
        <div className="item">
          <div>
            <label>P. Name</label>
            <input
              type="text"
              placeholder="Product"
              ref={(inputProdName) => {
                this.inputProdNameRef = inputProdName;
              }}
            ></input>
          </div>

          <br />
          <div>
            <label>P. Price</label>
            <input
              type="text"
              placeholder="Price"
              ref={(inputProdPrice) => {
                this.inputProdPriceRef = inputProdPrice;
              }}
            ></input>

            <button
              onClick={() => {
                addProduct({
                  name: this.inputProdNameRef.value,
                  price: this.inputProdPriceRef.value,
                });
              }}
            >
              Add
            </button>
          </div>
        </div>
      </div>
    );
  }
}


const mapStateToProps = (BaseComponent) => (props) => {
  const [addProduct] = UserStore(state => [state.addProduct], shallow);
  const store = {addProduct};
  return <BaseComponent {...props} store={store} />;
};
const EditPurchasesContainer = mapStateToProps(EditPurchases);
export default EditPurchasesContainer;
