import React, {useContext } from "react";

import "./footerUser.scss";
import UserStore from "../../store/userStore.js";
import RenderCounter from "../renderCounter/renderCounter.js";


export default function FooterUsers() {
  
  const userName = UserStore(state => state.userData.username);
  
  
  //console.log("state " + state)
  return (
    
        <div className="footer-container">
          <RenderCounter topLeft={true}/>
          <p className="username">{userName}</p>
        </div>
      
    
  );
}
