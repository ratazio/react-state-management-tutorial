import React, {useContext } from "react";

import "./headerUser.scss";
import UserStore from "../../store/userStore.js";
import RenderCounter from "../renderCounter/renderCounter.js";

export default function HeaderUsers() {
  
  const userName = UserStore(state => state.userData.username);
  const userAge = UserStore(state => state.userData.userage);
  

  return (
    <div className="header-container">
      <RenderCounter bottomLeft={true} />
      <p className="username"> {userName}</p>
      <p className="age">{userAge} yo</p>
    </div>
  );
}
