import React, {  } from "react";

import create from "zustand";

export var userInitalState = {
  userData: {
    username: "Usernamd 3s21",
    userage: 23,
  },
  purchaseHistory: [],
};

const useStore = create((set, get) => ({
  ...userInitalState,
  setName: (username) => {
    set((state) => ({
      userData: {
        username: username,
      },
    }));
  },
  setAge: (userage) => {
    set((state) => ({
      userData: {
        userage: userage,
      },
    }));
  },
  addProduct: (product) => {
    let newProduct = product;

    newProduct.id =
      get().purchaseHistory.length == 0
        ? 1
        : get().purchaseHistory[get().purchaseHistory.length - 1].id + 1;
    get().purchaseHistory.push(newProduct);

    let newUserStory = get().purchaseHistory;

    set((state) => ({
      purchaseHistory: newUserStory,
    }));
  },
  removeProduct: (id) => {
    
    let purchaseH = get().purchaseHistory;
    
    purchaseH = purchaseH.filter(
      (item) => item.id !== id
    );

    set((state) => ({
      purchaseHistory: purchaseH,
    }));
  },
}));

export default useStore;

export const withUserStore = (BaseComponent) => (props) => {
  const store = useStore();
  return <BaseComponent {...props} store={store} />;
};
