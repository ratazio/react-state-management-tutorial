import React from "react";
import "./renderCounter.scss";
import { Controls, PlayState, Tween } from 'react-gsap';
import {TweenLite} from 'gsap';

export default class RenderCounter extends React.Component {
  constructor() {
    super();

    this.renderCounter = 0;

    this.myElement = null;
    this.myTween = null;
  }

  componentDidMount(){
    // use the node ref to create the animation
    this.myTween = TweenLite.from(this.myElement, 1, {scale: 2.0, rotation: 18});
  }

  render(props) {
    let style = null;
    if (this.props.topRight) {
      style = { top: -15, right: 0 };
    } else if (this.props.topLeft) {
      style = { top: -15, right: 30 };
    } else if (this.props.bottomRight) {
      style = { top: 35, right: 0 };
    } else if (this.props.topLeft) {
      style = { top: -15, left: 0 };
    } else if (this.props.bottomLeft) {
      style = { top: 35, left: 0 };
    }

    this.renderCounter++;

    this.myTween = TweenLite.from(this.myElement, 1, {scale: 2.0, rotation: 18});

    return (
      
        <div 
        className="render-counter" 
        style={style} ref={div => this.myElement = div}
        >
          <p>{this.renderCounter}</p>
        </div>
     
    );
  }
}
