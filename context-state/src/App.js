import "./App.scss";
import HeaderUser from "./components/headerUser/headerUser.js";
import FooterUser from "./components/footerUser/footerUser.js";
import EditUser from "./components/editUser/editUser.js";
import NicknameHistory from "./components/nicknameHistory/nicknameHistory.js";
import RenderCounter from "./components/renderCounter/renderCounter.js"

import React, { useReducer, useContext, useEffect } from "react";
import UserContext, { userInitalState, UserContextWrapper, userReducer } from "./context/userContext.js";

var loadedFomBbbbbb = false;

function App() {
  const [state, dispatch] = useReducer(userReducer, userInitalState);

  useEffect(() => {
    if (!loadedFomBbbbbb) {
      /*
      const raw = localStorage.getItem("data");
      dispatch({ type: "init", payload: raw });
      loadedFomBbbbbb = true;
      */
    }
  });

  useEffect(() => {
    //localStorage.setItem("data", JSON.stringify(state));
  });

  return (
    <UserContextWrapper state_={state}>
      <div className="App">
        <header className="App-header">
          <HeaderUser />

          {/*
      <Context.Provider value={dispatch}>
        <button onClick={() => dispatch({ type: "add" })}>New Todo</button>
        <TodosList items={state} />
      </Context.Provider>
  */}
        </header>

        <RenderCounter bottomLeft={true}/>
        
        <div>
          <div className="user-container">
            <EditUser />
            <NicknameHistory />
          </div>
        </div>

        <footer>
          <FooterUser />
        </footer>
      </div>
    </UserContextWrapper>
  );
}
export default App;
