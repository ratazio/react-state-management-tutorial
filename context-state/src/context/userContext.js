import React, { Component, useReducer, useContext, useEffect } from "react";

//----
// initial state
//----
export var userInitalState = {
  userData: {
    username: "Username 3s21",
    userage: 23,
  },
  purchaseHistory: [
    {
      id: 0,
      productName: "opa 1",
      price: "32,00",
    },
  ],
};

//----
// declaration
//----
var UserContext = React.createContext({
  ...userInitalState,
  setName: (username) => {},
  setAge: (age) => {},
  addpurchaseToHistory: (productName) => {},
  removepurchaseToHistory: (productName) => {},
});

//----
// wrapper
//----
export class UserContextWrapper extends Component {
  constructor() {
    super();
    this.state = userInitalState;
  }

  setName = (username) => {

    console.log("set name state");
    this.setState({
      ...this.state,
      userData: {
        ...this.state.userData,
        username: username,
      },
    });
  };
  setAge = (age) => {
    this.setState({
      ...this.state,
      userData: {
        ...this.state.userData,
        userage: age,
      },
    });
  };

  addpurchaseToHistory = (productName) => {};

  removepurchaseToHistory = (productName) => {};

  render() {
    console.log("use context render");
    return (
      <UserContext.Provider
        value={{
          user: this.state,
          products: this.state.purchaseHistory,
          setName: this.setName,
          setAge: this.setAge,
        }}
      >
        {this.props.children}
      </UserContext.Provider>
    );
  }
}

export default UserContext;

export function userReducer(state, action) {
  switch (action.type) {
    case "get": {
      return [...state];
    }
    case "setName": {
      return [
      {
        ...this.state,
        userData: {
          ...this.state.userData,
          username: action.payload,
        },
      }]
      ;
    }
    case "setAge": {
      return [
      {
        ...this.state,
        userData: {
          ...this.state.userData,
          userage: action.payload,
        },
      }]
      ;
    }
    case "add": {
      return [
        ...state,
        {
          id: Date.now(),
          text: "",
          completed: false,
        },
      ];
    }
    case "delete": {
      console.log("delete ", action.payload);
      return state.filter((item) => item.id !== action.payload);
    }

    case "completed": {
      console.log("delete ", action.payload);
      return state.filter((item) => item.id !== action.payload);
    }

    default:
      break;
  }
}
