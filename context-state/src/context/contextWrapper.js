import React, { Component } from "react";
import UserContext from "./userContext";

class ContextWrapper extends Component {
  render() {
    return (
      <UserContext.Provider
        value={{
          user: userState,
          products: userState.purchaseHistory,
        }}
      >
        {this.props.children}
      </UserContext.Provider>
    );
  }
}

export default ContextWrapper;
