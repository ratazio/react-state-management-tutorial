import React from "react";
import "./headerUser.scss";
import UserContext from "../../context/userContext.js";
import RenderCounter from "../renderCounter/renderCounter.js"

export default function HeaderUsers() {
  
  return (
    <UserContext.Consumer>
        {(context) => (
          <div className="header-container">
            <RenderCounter bottomLeft={true}/>
            <p className="username"> {context.user.userData.username}</p>
            <p className="age">{context.user.userData.userage} yo</p>
          </div>
        )}
    </UserContext.Consumer>
  );

  /*
return (
    <UserContext.Consumer>
        {(context) => (
          <div className="header-container">
            <RenderCounter bottomLeft={true}/>
            <p className="username"> {context.user.userData.username}</p>
            <p className="age">{context.user.userData.userage} yo</p>
          </div>
        )}
    </UserContext.Consumer>
  );
  */
}
