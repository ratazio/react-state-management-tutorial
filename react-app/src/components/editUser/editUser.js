import React from "react";
import "./editUser.scss";
import UserContext from "../../context/userContext.js";
import RenderCounter from "../renderCounter/renderCounter.js"

export default class EditUser extends React.Component {
  constructor() {
    super();
    this.state = {
      username: "Username 123",
      userage: "23",
    };
  }
  onNameClick(onSetName) {
    onSetName(this.inputNameRef.value);
    console.log("onNameClick ", this.inputNameRef.value);
  }

  onAgeClick(onSetAge) {
    onSetAge(this.inputAgeRef.value);
    console.log("onAgeClick ", this.inputAgeRef.value);
  }

  onNicknameClick() {
    console.log("onNicknameClick ", this.inputNicknameRef.value);
  }

  render() {
    
    return (
      <UserContext.Consumer>
        {(context) => (
          
          
          
          <div className="edit-user">
          {
            <RenderCounter/>  
          }
            <h1>User</h1>
            <div className="item">
              <label>Name</label>
              <input
                type="text"
                placeholder="Username 123"
                ref={(inputName) => {
                  this.inputNameRef = inputName;
                }}
              ></input>
              <button onClick={this.onNameClick.bind(this, context.setName)}>Save</button>
            </div>
            <br />
            <div className="item">
              <label>Age</label>
              <input
                type="text"
                placeholder="23"
                ref={(inputAge) => {
                  this.inputAgeRef = inputAge;
                }}
              ></input>
              <button onClick={this.onAgeClick.bind(this, context.setAge)}>Save</button>
            </div>
            <div className="item">
              <label>Nickname</label>
              <input
                type="text"
                placeholder="Nickname"
                ref={(inputNickname) => {
                  this.inputNicknameRef = inputNickname;
                }}
              ></input>
              <button onClick={this.onNicknameClick.bind(this)}>Add</button>
            </div>
          </div>
        )}
      </UserContext.Consumer>
    );
  }
}
