import React from "react";
import "./footerUser.scss";
import UserContext from "../../context/userContext.js";
import RenderCounter from "../renderCounter/renderCounter.js"

export default function FooterUsers() {
  return (
    <UserContext.Consumer>
      {(context) => (
        <div className="footer-container">
          <RenderCounter topLeft={true}/>
          <p className="username">{context.user.userData.username}</p>
        </div>
      )}
    </UserContext.Consumer>
  );
}
