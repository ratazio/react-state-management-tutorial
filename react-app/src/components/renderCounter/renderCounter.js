import React from "react";
import "./renderCounter.scss";

export default class RenderCounter extends React.Component {
  constructor() {
    super();

    this.renderCounter = 0;
  }

  

  render(props) {
    let style = null;
    if(this.props.topRight){
      style = { top: -15, right: 0 }
    } else if(this.props.topLeft){
      style = { top: -15, right: 30 }
    } else if(this.props.bottomRight){
      style = { top: 35, right: 0 }
    } else if(this.props.topLeft){
      style = { top: -15, left: 0 }
    } else if(this.props.bottomLeft){
      style = { top: 35, left: 0 }
    }

    this.renderCounter++;

    
    
    return (
      <div className="render-counter" style={style}>
        <p>{this.renderCounter}</p>
      </div>
    );
  }
}
