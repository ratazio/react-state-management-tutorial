import React from "react";
import "./nicknameHistory.scss";
import RenderCounter from "../renderCounter/renderCounter.js"

export default class NicknameHistory extends React.Component {
  constructor() {
    super();
    this.state = {
      history: ["Nick 1", "Nick 2", "Nick 3"],
    };
  }

  render() {
    return (
      <div className="nickname-history">
        <RenderCounter bottomLeft={true}/>
        <h1>History</h1>
        {this.state.history.map(function (item, i) {
          return <p key={i}>Item {i}</p>
        })}
      </div>
    );
  }
}
