import "./App.scss";
import HeaderUser from "./components/headerUser/headerUser.js";
import FooterUser from "./components/footerUser/footerUser.js";
import EditUser from "./components/editUser/editUser.js";
import NicknameHistory from "./components/nicknameHistory/nicknameHistory.js";

import React, { useReducer } from "react";

function appReducer(state, action) {
  switch (action.type) {
    case "add": {
      return [
        ...state,
        {
          id: Date.now(),
          text: "",
          completed: false,
        },
      ];
    }
    case "delete": {
      return state.filter((item) => item.id !== action.payload);
    }

    default:
      break;
  }
}

function App() {
  const [state, dispatch] = useReducer(appReducer, []);
  return (
    <div className="App">
      
      <header className="App-header">
      <HeaderUser />
      </header>

      <div>
      <div className="user-container">
        <EditUser />
        <NicknameHistory />
      </div>
      </div>

      <footer>
        <FooterUser />
      </footer>
      

      <button onClick={() => dispatch({ type: "add" })}>New Todo</button>
      <TodosList items={state} />
    </div>
  );
}

function TodosList({ items }) {
  return items.map((item) => <TodoItem key={item.id} {...item} />);
}

function ee(e) {}

function cc(e) {}
function TodoItem({ id, completed, text }) {
  return (
    <div>
      <input type="checkbox " onChange={ee} defaultChecked={true} />

      <input type="text" defaultValue={text} />
    </div>
  );
}
export default App;
