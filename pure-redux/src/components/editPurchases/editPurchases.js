import React from "react";
import { useSelector, useDispatch, connect } from "react-redux";
import "./editPurchases.scss";
import UserContext from "../../store/userStore.js";
import RenderCounter from "../renderCounter/renderCounter.js";

class EditPurchases extends React.Component {
  constructor() {
    super();
    this.state = {
      username: "Username 123",
      userage: "23",
    };
  }

  onNicknameClick() {
    console.log("onNicknameClick ", this.inputNicknameRef.value);
  }

  render() {
    return (
      <div className="edit-purchases">
        {<RenderCounter />}

        <h1>Product</h1>
        <div className="item">
          <div>
            <label>P. Name</label>
            <input
              type="text"
              placeholder="Product"
              ref={(inputProdName) => {
                this.inputProdNameRef = inputProdName;
              }}
            ></input>
          </div>

          <br />
          <div>
            <label>P. Price</label>
            <input
              type="text"
              placeholder="Price"
              ref={(inputProdPrice) => {
                this.inputProdPriceRef = inputProdPrice;
              }}
            ></input>

            <button
              onClick={() => {
                this.props.addProduct({
                  name: this.inputProdNameRef.value,
                  price: this.inputProdPriceRef.value,
                });
              }}
            >
              Add
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addProduct: (product) => {
      dispatch({ type: "addProduct", payload: product });
    },
    removeProduct: (id) => {
      dispatch({ type: "delete", payload: id });
    },
  };
};

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(EditPurchases);
