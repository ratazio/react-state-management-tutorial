import React, {useContext } from "react";
import { shallowEqual, useSelector, useDispatch, connect } from "react-redux";
import "./footerUser.scss";
//import UserContext from "../../context/userContext.js";
import RenderCounter from "../renderCounter/renderCounter.js"

export default function FooterUsers() {

  const userName = useSelector(state => state.userData.username)
  
  
  //console.log("state " + state)
  return (
    
        <div className="footer-container">
          <RenderCounter topLeft={true}/>
          <p className="username">{userName}</p>
        </div>
      
    
  );
}
