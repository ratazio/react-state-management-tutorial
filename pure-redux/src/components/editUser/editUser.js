import React from "react";
import { shallowEqual, useSelector, useDispatch, connect } from "react-redux";
import "./editUser.scss";
import UserContext, { UserDispatchContext } from "../../store/userStore.js";
import RenderCounter from "../renderCounter/renderCounter.js";

class EditUser extends React.Component {
  constructor() {
    super();
    this.state = {
      username: "Username 123",
      userage: "23",
    };
  }


  render() {
    const {setName, setAge} = this.props;//useDispatch();

    return (
      <div className="edit-user">
        {<RenderCounter />}
        <h1>User</h1>
        <br></br>
        <div className="item">
          <label>Name</label>
          <input
            type="text"
            placeholder="Username 123"
            ref={(inputName) => {
              this.inputNameRef = inputName;
            }}
          ></input>
          <button
            onClick={() => {
              setName(this.inputNameRef.value);
            }}
          >
            Save
          </button>
        </div>
        <br />
        <div className="item">
          <label>Age</label>
          <input
            type="text"
            placeholder="23"
            ref={(inputAge) => {
              this.inputAgeRef = inputAge;
            }}
          ></input>
          <button
            onClick={() => {
              setAge(this.inputAgeRef.value);
            }}
          >
            Save
          </button>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // dispatching plain actions
    setName: (name) => {
      dispatch({ type: "setName", payload:name });
    },
    setAge: (age) => {
      dispatch({ type: "setAge", payload:age });
    },
    reset: () => dispatch({ type: "RESET" }),
  };
};

const mapStateToProps = (state) => {
  return { }
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditUser)