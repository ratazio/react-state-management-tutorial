import React from "react";
import { connect, useSelector, useDispatch } from "react-redux";
import "./nicknameHistory.scss";
import UserContext from "../../store/userStore.js";
import RenderCounter from "../renderCounter/renderCounter.js";

class NicknameHistory extends React.Component {
  constructor() {
    super();
    this.state = {
    };
  }


  render() {
    const {purchaseHistory, deleteProduct} = this.props;
    console.log("render to purchaseHistory: " , purchaseHistory)
    return (
      <div className="nickname-history">
        <RenderCounter bottomLeft={true} />
        <h1>History</h1>
        {purchaseHistory.map(function (item, i) {
          return (
            <div className="line">
              <p key={i}>
                ID: {item.id} Name: {item.name} Price: {item.price}
              </p>
              <button onClick={() => {
              deleteProduct(item.id);
            }}>Delete</button>
            </div>
          );
        })}
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  


  //let purchaseHistory = useSelector(state => state.purchaseHistory)
  let purchaseHistory = state.purchaseHistory;

  console.log("state to purchaseHistory: " , purchaseHistory)
  return {purchaseHistory:purchaseHistory};
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteProduct:(id) => {
      dispatch({type:"delete", payload: id});
  },
}
}
export default connect (mapStateToProps, mapDispatchToProps) (NicknameHistory)