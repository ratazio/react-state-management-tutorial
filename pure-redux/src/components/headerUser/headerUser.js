import React, {useContext } from "react";
import { useSelector } from "react-redux";
import "./headerUser.scss";
import UserContext from "../../store/userStore.js";
import RenderCounter from "../renderCounter/renderCounter.js";

export default function HeaderUsers() {
  const userName = useSelector(state => state.userData.username)
  const userAge = useSelector(state => state.userData.userage)
  return (
    <div className="header-container">
      <RenderCounter bottomLeft={true} />
      <p className="username"> {userName}</p>
      <p className="age">{userAge} yo</p>
    </div>
  );
}
