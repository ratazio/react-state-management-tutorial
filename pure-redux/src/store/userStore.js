import React, { Component, useReducer, useContext, useEffect } from "react";
import { createStore } from "redux";

export var userInitalState = {
  userData: {
    username: "Usernamedd 3s21",
    userage: 23,
  },
  purchaseHistory: [],
};

const userReducer = (state = userInitalState, action) => {
  switch (action.type) {
    case "setName": {
      let result = { ...state };
      result.userData.username = action.payload;
      return result;
    }
    case "setAge": {
      let result = { ...state };
      result.userData.userage = action.payload;
      return result;
    }
    case "addProduct": {
      let result = { ...state };
      
      let product = action.payload;

      product.id =
        result.purchaseHistory.length == 0
          ? 1
          : result.purchaseHistory[result.purchaseHistory.length - 1].id + 1;
      result.purchaseHistory.push(product);
      const deepCloneOfNestedObject = JSON.parse(JSON.stringify(result))

      console.log("adding product")
      return deepCloneOfNestedObject;
    }

    case "delete": {
      let result = { ...state };
      let id = action.payload;
      console.log("delete ", action.payload);
      result.purchaseHistory = state.purchaseHistory.filter(
        (item) => item.id !== id
      );
      return result;
    }

    default:
      return state;
      break;
  }
}

let UserStore = createStore(userReducer);



//----
// initial state
//----

//----
// wrapper
//----
export class UserContextWrapper extends Component {
  render() {
    return <div>{this.props.children}</div>;
  }
}

export default UserStore;
