import "./App.scss";
import HeaderUser from "./components/headerUser/headerUser.js";
import FooterUser from "./components/footerUser/footerUser.js";
import EditUser from "./components/editUser/editUser.js";
import EditPurchases from "./components/editPurchases/editPurchases.js";
import NicknameHistory from "./components/nicknameHistory/nicknameHistory.js";
import RenderCounter from "./components/renderCounter/renderCounter.js";
import { useImmerReducer } from "use-immer";
import React, { useReducer, useContext, useEffect } from "react";
import { Provider, useSelector } from "react-redux";
import UserStore from "./store/userStore.js";

var loadedFomBbbbbb = false;

function App() {
  return (
    <div>
      <div className="App">
        <header className="App-header">
          <HeaderUser />
        </header>

        <RenderCounter bottomLeft={true} />

        <div>
          <div className="user-container">
            <EditUser />
            <EditPurchases />
          </div>
        </div>

        <NicknameHistory />

        <footer>
          <FooterUser />
        </footer>
      </div>
    </div>
  );
}

export default App;
