import React from "react";
import "./editUser.scss";
import UserContext, {UserDispatchContext} from "../../context/userContext.js";
import RenderCounter from "../renderCounter/renderCounter.js";

export default class EditUser extends React.Component {
  constructor() {
    super();
    this.state = {
      username: "Username 123",
      userage: "23",
    };
  }
  static contextType = UserContext;

  onNicknameClick() {
    console.log("onNicknameClick ", this.inputNicknameRef.value);
  }

  render() {
    const { dispatch } = this.context;

    return (
      <div className="edit-user">
        {<RenderCounter />}
        <h1>User</h1>
        <div className="item">
          <label>Name</label>
          <input
            type="text"
            placeholder="Username 123"
            ref={(inputName) => {
              this.inputNameRef = inputName;
            }}
          ></input>
          <button
            onClick={() => {
              dispatch({ type: "setName", payload: this.inputNameRef.value });
            }}
          >
            Save
          </button>
        </div>
        <br />
        <div className="item">
          <label>Age</label>
          <input
            type="text"
            placeholder="23"
            ref={(inputAge) => {
              this.inputAgeRef = inputAge;
            }}
          ></input>
          <button
            onClick={() => {
              dispatch({ type: "setAge", payload: this.inputAgeRef.value });
            }}
          >
            Save
          </button>
        </div>
        
      </div>
    );
  }
}
