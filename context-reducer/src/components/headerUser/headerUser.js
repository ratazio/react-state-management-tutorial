import React, {useContext } from "react";
import "./headerUser.scss";
import UserContext from "../../context/userContext.js";
import RenderCounter from "../renderCounter/renderCounter.js";

export default function HeaderUsers() {
  const { state, dispatch } = useContext(UserContext);
  const { username, userage } = state.userData;
  return (
    <div className="header-container">
      <RenderCounter bottomLeft={true} />
      <p className="username"> {username}</p>
      <p className="age">{userage} yo</p>
    </div>
  );
}
