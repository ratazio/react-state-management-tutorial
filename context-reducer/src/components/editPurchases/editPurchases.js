import React from "react";
import "./editPurchases.scss";
import UserContext, {  } from "../../context/userContext.js";
import RenderCounter from "../renderCounter/renderCounter.js";

export default class EditPurchases extends React.Component {
  constructor() {
    super();
    this.state = {
      username: "Username 123",
      userage: "23",
    };
  }
  static contextType = UserContext;

  onNicknameClick() {
    console.log("onNicknameClick ", this.inputNicknameRef.value);
  }

  render() {
    const { dispatch } = this.context;

    return (
      <div className="edit-purchases">
        {<RenderCounter />}

        <h1>Product</h1>
        <div className="item">
          <label>P. Name</label>
          <input
            type="text"
            placeholder="Product"
            ref={(inputProdName) => {
              this.inputProdNameRef = inputProdName;
            }}
          ></input>
          <br />
          <label>P. Price</label>
          <input
            type="text"
            placeholder="Price"
            ref={(inputProdPrice) => {
              this.inputProdPriceRef = inputProdPrice;
            }}
          ></input>

          <button
            onClick={() => {
              dispatch({
                type: "addProduct",
                payload: {
                  name: this.inputProdNameRef.value,
                  price: this.inputProdPriceRef.value,
                },
              });
            }}
          >
            Add
          </button>
        </div>
      </div>
    );
  }
}
