import React from "react";
import "./nicknameHistory.scss";
import UserContext from "../../context/userContext.js";
import RenderCounter from "../renderCounter/renderCounter.js";

export default class NicknameHistory extends React.Component {
  constructor() {
    super();
  }

  static contextType = UserContext;

  render() {
    const { state, dispatch } = this.context;
    const { purchaseHistory } = state;

    return (
      <div className="nickname-history">
        <RenderCounter bottomLeft={true} />
        <h1>History</h1>
        {purchaseHistory.map(function (item, i) {
          return (
            <div className="line">
              <p key={i}>
                ID: {item.id} Name: {item.name} Price: {item.price}
              </p>
              <button onClick={() => {
              dispatch({ type: "delete", payload: item.id });
            }}>Delete</button>
            </div>
          );
        })}
      </div>
    );
  }
}
