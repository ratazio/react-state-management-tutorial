import React, {useContext } from "react";
import "./footerUser.scss";
import UserContext from "../../context/userContext.js";
import RenderCounter from "../renderCounter/renderCounter.js"

export default function FooterUsers() {
  const { state, dispatch } = useContext(UserContext)
  return (
    
        <div className="footer-container">
          <RenderCounter topLeft={true}/>
          <p className="username">{state.userData.username}</p>
        </div>
      
    
  );
}
