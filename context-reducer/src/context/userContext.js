import React, { Component, useReducer, useContext, useEffect } from "react";

//----
// initial state
//----
export var userInitalState = {
  userData: {
    username: "Usernamedd 3s21",
    userage: 23,
  },
  purchaseHistory: [],
};

export var purchasesInitalState = {
  purchaseHistory: [],
};

//----
// declaration
//----
export var UserContext = React.createContext();
//export var UserDispatchContext = React.createContext();

//----
// wrapper
//----
export class UserContextWrapper extends Component {
  render() {
    return (
      <UserContext.Provider
        value={{ state: this.props.state, dispatch: this.props.dispatch }}
      >
        {this.props.children}
      </UserContext.Provider>
    );
  }
}

export default UserContext;

export function userReducer(state, action) {
  switch (action.type) {
    case "setName": {
      let result = { ...state };
      result.userData.username = action.payload;
      return result;
    }
    case "setAge": {
      let result = { ...state };
      result.userData.userage = action.payload;
      return result;
    }
    case "addProduct": {
      let result = { ...state };
      let product = action.payload;
      
      product.id = result.purchaseHistory.length == 0?1:result.purchaseHistory[result.purchaseHistory.length-1].id+1;
      result.purchaseHistory.push(product);
      return result;
    }

    case "delete": {
      let result = { ...state };
      let id = action.payload;
      console.log("delete ", action.payload);
      result.purchaseHistory =  state.purchaseHistory.filter((item) => item.id !== id);
      return result;
    }

    default:
      break;
  }
}
